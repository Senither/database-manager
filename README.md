Database Manager
================

Created by [Alexis Tan (Senither)](http://senither.com/), inspired heavly by [Taylor Otwells](https://twitter.com/taylorotwell) PHP Framework [Laravel](http://laravel.com/), and [Jeffrey Ways](https://twitter.com/jeffrey_way) programming courses at [Laracasts](https://laracasts.com/).

## Links to stuff

| Name              | Link                                                                                                                                                                               |
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Source Code       | [https://bitbucket.org/Senither/database-manager/src](https://bitbucket.org/Senither/database-manager/src)                                                                         |
| Pull Requests     | [https://bitbucket.org/Senither/database-manager/pull-requests/](https://bitbucket.org/Senither/database-manager/pull-requests/)                                                   |
| Issues            | [https://bitbucket.org/Senither/database-manager/issues](https://bitbucket.org/Senither/database-manager/issues)                                                                   |
| Wiki              | [https://bitbucket.org/Senither/database-manager/wiki](https://bitbucket.org/Senither/database-manager/wiki)                                                                       |
| Graph             | [https://bitbucket.org/Senither/database-manager/addon/bitbucket-graphs/graphs-repo-page](https://bitbucket.org/Senither/database-manager/addon/bitbucket-graphs/graphs-repo-page) |
| Sen Dev Dashboard | [http://sen-dev.com/client/dashboard](http://sen-dev.com/client/dashboard)                                                                                                         |